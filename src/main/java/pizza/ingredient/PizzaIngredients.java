package pizza.ingredient;

public class PizzaIngredients {

  private String dough;
  private String sauce;
  private String toppings;

  public PizzaIngredients(String dough, String sauce, String toppings) {
    this.dough = dough;
    this.sauce = sauce;
    this.toppings = toppings;
  }

  @Override
  public String toString() {
    return "Dough: " + dough + "\n"
        + "Sauce: " + sauce + "\n"
        + "Toppings: " + toppings;
  }
}
