package pizza.enums;

public enum PizzaType {
  PEPERONI(1), CHEESE(2), CLAM(3), VEGIE(4);
  private int pizzaNumber;

  private PizzaType(int pizzaNumber) {
    this.pizzaNumber = pizzaNumber;
  }

  public int getPizzaNumber() {
    return pizzaNumber;
  }

}
