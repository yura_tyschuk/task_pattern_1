package factory;

import java.util.List;
import pizza.enums.PizzaType;
import pizza.ingredient.PizzaIngredients;
import pizzas.Pizza;

public abstract class PizzaCafe {

  protected abstract Pizza createPizza(PizzaType pizzaType);


  public Pizza prepare(PizzaType pizzaType,
      List<PizzaIngredients> ingredients) {
    Pizza pizza = createPizza(pizzaType);
    pizza.prepare(ingredients);
    pizza.bake();
    pizza.cut();
    pizza.box();

    return pizza;
  }
}
