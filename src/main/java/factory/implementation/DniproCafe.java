package factory.implementation;

import factory.PizzaCafe;
import pizza.enums.PizzaType;
import pizzas.Pizza;
import pizzas.implementation.CheesePizza;
import pizzas.implementation.ClamPizza;
import pizzas.implementation.PeperoniPizza;
import pizzas.implementation.VegiePizza;

public class DniproCafe extends PizzaCafe {

  @Override
  protected Pizza createPizza(PizzaType pizzaType) {
    Pizza pizza = null;
    if (pizzaType == PizzaType.CHEESE) {
      pizza = new CheesePizza();
    } else if (pizzaType == pizzaType.PEPERONI) {
      pizza = new PeperoniPizza();
    } else if (pizzaType == pizzaType.CLAM) {
      pizza = new ClamPizza();
    } else if (pizzaType == pizzaType.VEGIE) {
      pizza = new VegiePizza();
    }
    return pizza;
  }
}
