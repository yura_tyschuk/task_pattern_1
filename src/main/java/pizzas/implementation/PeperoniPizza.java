package pizzas.implementation;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizza.ingredient.PizzaIngredients;
import pizzas.Pizza;

public class PeperoniPizza implements Pizza {

  private static Logger logger = LogManager.getLogger();

  @Override
  public void prepare(List<PizzaIngredients> ingredients) {

    logger.warn("Ingredients are: " + ingredients);
    logger.warn("Peperoni pizza is prepared");
  }

  @Override
  public void bake() {
    logger.warn("Peperoni pizza is baking");
  }

  @Override
  public void cut() {
    logger.warn("Peperoni pizza is cutting");
  }

  @Override
  public void box() {
    logger.warn("Peperoni pizza is boxed");
  }
}
