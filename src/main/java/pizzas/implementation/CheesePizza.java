package pizzas.implementation;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizza.ingredient.PizzaIngredients;
import pizzas.Pizza;

public class CheesePizza implements Pizza {

  private static Logger logger = LogManager.getLogger();

  @Override
  public void prepare(List<PizzaIngredients> ingredients) {
    logger.warn("Ingredients are: " + ingredients);
    logger.warn("Cheese pizza is prepared");
  }

  @Override
  public void bake() {
    logger.warn("Cheese pizza is baked");
  }

  @Override
  public void cut() {
    logger.warn("Cheese pizza is cut");
  }

  @Override
  public void box() {
    logger.warn("Chesse piza is boxed");
  }
}
