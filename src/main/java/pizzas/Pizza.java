package pizzas;

import java.util.List;
import pizza.ingredient.PizzaIngredients;

public interface Pizza {

  void prepare(List<PizzaIngredients> ingredients);

  void bake();

  void cut();

  void box();
}
