package menu;

import controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pizza.enums.PizzaType;

public class MyView {

  private PizzaType pizzaType;
  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private static Logger logger = LogManager.getLogger();

  public MyView() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    logger.warn("Choose your city: ");
    menu.put("1", "1 - Kyiv");
    menu.put("2", "2 - Dnirpo");
    menu.put("3", "3 - Lviv");
    menu.put("Q", "Quit");

    methodsMenu.put("1", this::kyivCafe);
    methodsMenu.put("2", this::dniproCafe);
    methodsMenu.put("3", this::lvivCafe);
  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private PizzaType choosePizzaType() {
    logger.warn("Choose pizza type: ");
    logger.warn("Peperoni - 1, Cheese - 2, Clam - 3, Vegie - 4");
    int pizzaNumber = input.nextInt();

    if (pizzaNumber == 1) {
      return PizzaType.PEPERONI;
    } else if (pizzaNumber == 2) {
      return PizzaType.CHEESE;
    } else if (pizzaNumber == 3) {
      return PizzaType.CLAM;
    } else if (pizzaNumber == 4) {
      return PizzaType.VEGIE;
    } else {
      logger.warn("You print incorrect number");
    }
    return null;
  }

  private void kyivCafe() {
    pizzaType = choosePizzaType();
    controller.kyivCafe(pizzaType);
  }

  private void dniproCafe() {
    pizzaType = choosePizzaType();
    controller.dniproCafe(pizzaType);
  }

  private void lvivCafe() {
    pizzaType = choosePizzaType();
    controller.lvivCafe(pizzaType);
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}
