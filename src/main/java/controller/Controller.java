package controller;

import factory.PizzaCafe;
import factory.implementation.DniproCafe;
import factory.implementation.KyivCafe;
import factory.implementation.LvivCafe;
import java.util.ArrayList;
import java.util.List;
import pizza.enums.PizzaType;
import pizza.ingredient.PizzaIngredients;
import pizzas.Pizza;

public class Controller {

  List<PizzaIngredients> pizzaIngredientsList = new ArrayList<>();


  public Controller() {
    PizzaIngredients pizzaIngredients = new PizzaIngredients("thick",
        "marinara", "toppings");
    pizzaIngredientsList.add(pizzaIngredients);

  }

  public void dniproCafe(PizzaType pizzaType) {
    PizzaCafe pizzaCafe = new DniproCafe();
    Pizza pizza = pizzaCafe.prepare(pizzaType, pizzaIngredientsList);

  }

  public void lvivCafe(PizzaType pizzaType) {
    PizzaCafe pizzaCafe = new LvivCafe();
    Pizza pizza = pizzaCafe.prepare(pizzaType, pizzaIngredientsList);

  }

  public void kyivCafe(PizzaType pizzaType) {
    PizzaCafe pizzaCafe = new KyivCafe();
    Pizza pizza = pizzaCafe.prepare(pizzaType, pizzaIngredientsList);

  }
}
